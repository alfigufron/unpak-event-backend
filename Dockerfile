FROM node:16-alpine

WORKDIR /app/unpak-event-backend

COPY . /app/unpak-event-backend

RUN npm install

RUN npm run build

CMD ["node", "build/app.js"]