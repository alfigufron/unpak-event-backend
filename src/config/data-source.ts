import { DataSource } from "typeorm";
import Event from "../entity/Event";
import Ticket from "../entity/Ticket";
import User from "../entity/User";
import Vanue from "../entity/Vanue";
import env from "./env";

const AppDataSource = new DataSource({
  type: "mysql",
  host: env.DB.HOST,
  username: env.DB.USERNAME,
  password: env.DB.PASSWORD,
  port: env.DB.PORT,
  database: env.DB.NAME,
  entities: [Event, Ticket, User, Vanue],
});

export default AppDataSource;
