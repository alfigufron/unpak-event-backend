import { Request, Response, Router } from "express";
import EventController from "../controllers/event.controller";
import TicketController from "../controllers/ticket.controller";
import UserController from "../controllers/user.controller";
import VanueController from "../controllers/vanue.controller";

const route = Router();

export default function apiRouter() {
  route.get("/ping", (req: Request, res: Response) => res.send("Service OK!"));

  /**
   * API Version 1
   */
  const apiv1Router = () => {
    route.get("/vanue", VanueController.all);
    route.post("/vanue", VanueController.create);
    route.get("/vanue/:id", VanueController.detail);
    route.put("/vanue/:id", VanueController.update);
    route.delete("/vanue/:id", VanueController.delete);

    route.get("/event", EventController.all);
    route.post("/event", EventController.create);
    route.get("/event/:id", EventController.detail);
    route.put("/event/:id", EventController.update);
    route.delete("/event/:id", EventController.delete);

    route.get("/user", UserController.all);
    route.post("/user", UserController.create);
    route.get("/user/:id", UserController.detail);
    route.put("/user/:id", UserController.update);
    route.delete("/user/:id", UserController.delete);

    route.get("/ticket", TicketController.all);
    route.post("/ticket", TicketController.create);
    route.get("/ticket/:id", TicketController.detail);
    route.put("/ticket/:id", TicketController.update);
    route.delete("/ticket/:id", TicketController.delete);

    return route;
  };

  route.use("/api/v1", apiv1Router());

  return route;
}
