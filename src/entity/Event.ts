import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "events" })
export default class Event extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  location: string;

  @Column()
  date: string;

  @Column()
  duration: string;

  @Column({
    type: "bigint",
    unsigned: true,
  })
  vanue_id: number;
}
