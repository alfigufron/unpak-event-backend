import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "tickets" })
export default class Ticket extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  price: number;

  @Column({
    type: "bigint",
    unsigned: true,
  })
  event_id: number;

  @Column({
    type: "bigint",
    unsigned: true,
  })
  user_id: number;
}
