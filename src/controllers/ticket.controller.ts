import { NextFunction, Request, Response } from "express";
import { ErrorHandler, HttpResponse } from "../config/http";
import { HTTPCode } from "../constant/http.constant";
import Event from "../entity/Event";
import Ticket from "../entity/Ticket";
import User from "../entity/User";

export default class TicketController {
  public static async all(req: Request, res: Response, next: NextFunction) {
    try {
      const tickets = await Ticket.find();

      return HttpResponse.success(res, "List Ticket", tickets);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { date, price, event_id, user_id } = req.body;

      const event = await Event.findOneBy({ id: Number(event_id) });
      if (!event)
        throw new ErrorHandler("Event Not Found", null, HTTPCode.NotFound);

      const user = await User.findOneBy({ id: Number(user_id) });
      if (!user)
        throw new ErrorHandler("User Not Found", null, HTTPCode.NotFound);

      const ticket = new Ticket();
      ticket.date = date;
      ticket.price = price;
      ticket.event_id = event_id;
      ticket.user_id = user_id;

      await ticket.save();

      return HttpResponse.success(res, "Create Ticket Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async detail(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const ticket = await Ticket.findOneBy({ id: Number(id) });

      if (!ticket)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      return HttpResponse.success(res, "Detail Ticket", ticket);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { date, price, event_id, user_id } = req.body;

      const ticket = await Ticket.findOneBy({ id: Number(id) });

      if (!ticket)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      ticket.date = date;
      ticket.price = price;
      ticket.event_id = event_id;
      ticket.user_id = user_id;
      ticket.save();

      return HttpResponse.success(res, "Update Ticket Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const ticket = await Ticket.findOneBy({ id: Number(id) });

      if (!ticket)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      ticket.remove();

      return HttpResponse.success(res, "Delete Ticket Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }
}
