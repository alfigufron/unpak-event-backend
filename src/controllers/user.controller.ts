import { NextFunction, Request, Response } from "express";
import { ErrorHandler, HttpResponse } from "../config/http";
import { HTTPCode } from "../constant/http.constant";
import User from "../entity/User";

export default class UserController {
  public static async all(req: Request, res: Response, next: NextFunction) {
    try {
      const users = await User.find();

      return HttpResponse.success(res, "List User", users);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, email, phone_number, gender } = req.body;

      const user = new User();
      user.name = name;
      user.email = email;
      user.phone_number = phone_number;
      user.gender = gender;

      await user.save();

      return HttpResponse.success(res, "Create User Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async detail(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const user = await User.findOneBy({ id: Number(id) });

      if (!user)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      return HttpResponse.success(res, "Detail User", user);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name, email, phone_number, gender } = req.body;

      const user = await User.findOneBy({ id: Number(id) });

      if (!user)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      user.name = name;
      user.email = email;
      user.phone_number = phone_number;
      user.gender = gender;
      user.save();

      return HttpResponse.success(res, "Update User Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const user = await User.findOneBy({ id: Number(id) });

      if (!user)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      user.remove();

      return HttpResponse.success(res, "Delete User Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }
}
