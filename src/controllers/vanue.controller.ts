import { NextFunction, Request, Response } from "express";
import { ErrorHandler, HttpResponse } from "../config/http";
import { HTTPCode } from "../constant/http.constant";
import Vanue from "../entity/Vanue";

export default class VanueController {
  public static async all(req: Request, res: Response, next: NextFunction) {
    try {
      const vanues = await Vanue.find();

      return HttpResponse.success(res, "List Vanue", vanues);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, address } = req.body;

      const vanue = new Vanue();
      vanue.name = name;
      vanue.address = address;

      await vanue.save();

      return HttpResponse.success(res, "Create Vanue Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async detail(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const vanue = await Vanue.findOneBy({ id: Number(id) });

      if (!vanue)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      return HttpResponse.success(res, "Detail Vanue", vanue);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name, address } = req.body;

      const vanue = await Vanue.findOneBy({ id: Number(id) });

      if (!vanue)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      vanue.name = name;
      vanue.address = address;
      vanue.save();

      return HttpResponse.success(res, "Update Vanue Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const vanue = await Vanue.findOneBy({ id: Number(id) });

      if (!vanue)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      vanue.remove();

      return HttpResponse.success(res, "Delete Vanue Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }
}
