import { NextFunction, Request, Response } from "express";
import { ErrorHandler, HttpResponse } from "../config/http";
import { HTTPCode } from "../constant/http.constant";
import Event from "../entity/Event";
import Vanue from "../entity/Vanue";

export default class EventController {
  public static async all(req: Request, res: Response, next: NextFunction) {
    try {
      const events = await Event.find();

      return HttpResponse.success(res, "List Event", events);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, location, date, duration, vanue_id } = req.body;

      const vanue = await Vanue.findOneBy({ id: Number(vanue_id) });
      if (!vanue)
        throw new ErrorHandler("Vanue Not Found", null, HTTPCode.NotFound);

      const event = new Event();
      event.name = name;
      event.location = location;
      event.date = date;
      event.duration = duration;
      event.vanue_id = vanue_id;

      await event.save();

      return HttpResponse.success(res, "Create Event Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async detail(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const event = await Event.findOneBy({ id: Number(id) });

      if (!event)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      return HttpResponse.success(res, "Detail Event", event);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const { name, location, date, duration, vanue_id } = req.body;

      const vanue = await Vanue.findOneBy({ id: Number(vanue_id) });
      if (!vanue)
        throw new ErrorHandler("Vanue Not Found", null, HTTPCode.NotFound);

      const event = await Event.findOneBy({ id: Number(id) });

      if (!event)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      event.name = name;
      event.location = location;
      event.date = date;
      event.duration = duration;
      event.vanue_id = vanue_id;
      event.save();

      return HttpResponse.success(res, "Update Event Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const event = await Event.findOneBy({ id: Number(id) });

      if (!event)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      event.remove();

      return HttpResponse.success(res, "Delete Event Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }
}
